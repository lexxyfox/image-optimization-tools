#!

# PREREQUISITES
# gcc, git, gzexe, pkgconfig(libpng), pkgconfig(zlib)

e='/tmp/tmp.f56667da'
[ -f "$e" ] || {
  a="$(mktemp -d)"; trap "rm -rf \"$a\"" EXIT
  git clone https://git.code.sf.net/p/pmt/code -b pngcrush --depth 1 --single-branch --no-tags "$a"
  b="$(mktemp)"; trap "rm -f \"$b\"" EXIT
  gcc -c -I/usr/local/include -g -O3 -fomit-frame-pointer -Wall "$a/pngcrush.c" -o "$b"
  gcc -o "$e" "$b" -L/usr/local/lib -L/usr/local/lib -lpng -lz -lm
  gzexe "$e"; rm "$e~"
}

a="$(mktemp)"; trap "rm -f \"$a\"" EXIT
time "$e" -v -blacken -brute -reduce -rem alla /dev/stdin "$a" 2>&1 |
tail -n+19 1>&2; cat "$a"

